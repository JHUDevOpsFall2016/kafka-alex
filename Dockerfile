FROM  centos:7
MAINTAINER  Alex Goodman

# Install build and run dependencies for Kafka
RUN yum install -y wget java-1.7.0-openjdk-devel gradle
